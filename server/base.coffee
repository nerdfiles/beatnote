###*
# Module dependencies.
###

debug = require('debug')('dual-example')
_ = require('lodash')
Promise = require('bluebird')
express = require('express')
http = require('http')
config = port: process.env['PORT'] or 3000
Promise.resolve(express()).then((app) ->
  app.set 'port', config.port
  app
).then(require('../app/logger')).then(require('../app/express-middleware')).then(require('../app/static')).then(require('../app/dual')).then(require('../app/handlers')).then (app) ->
  server = http.createServer(app)
  app.get('io').attach server
  server.listen app.get('port'), ->
    console.log 'Express server listening on port ' + server.address().port
    return
  return
