define ['angularAMD', 'routes', 'ngDb'], (angularAMD, routes) ->
  __interface__ = angular.module('__interface__', [
    'ngDb'
  ])
  angularAMD.bootstrap(__interface__)

