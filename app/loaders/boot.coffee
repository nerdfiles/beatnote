_paths =
  'io-client'  : ''
  'lovefield'  : ''
  'angularAMD' : ''
  'Modernizr'  : ''
  'lodash'     : ''
  'angular'    : ''
  'ngDb'       : ''
  'engine.io-client': ''
  'dualapi': ''
  'dual-engine.io-client': ''
  'channels': ''

_shim =
  'angular'   : ' exports' : ' angular'
  'Modernizr' : ' exports' : ' Modernizr'
  'lovefield' : ' exports' : ' lovefield'
  'lodash'    : ' exports' : ' _'

  'angularAMD' : 'deps' : ['angular']
  'ngDb'       : 'deps' : ['angular']

  'dualapi': ['dual-engine.io-client']
  'channels': ['engine.io-client', 'dual-engine.io-client']

  'controller/home_ctrl': []

require.config
  baseUrl : "/",
  paths   : _paths
  shim    : _shim
  deps    : ['channels', 'interface']
