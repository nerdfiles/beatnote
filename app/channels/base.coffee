define ['engine.io-client', 'dualapi'], (eio, dualapi) ->
  d = dualapi().mount(
    '**': (body, ctxt) ->
      console.log ctxt.from.join('/'), '->', ctxt.to.join('/'), ':', body
      return
    addend: (body) ->
      console.log(body)
      return
    connect: server: ->
      statusText.innerText = 'connected'
      d.request([
        'server'
        'addend'
      ]).spread (body, options) ->
        if options.statusCode == 200
          console.log(body)
        else
          console.error '' + options.statusCode + ' Server did not return addend'
        return
      return
    disconnect: server: ->
      return
  )
  d.engineio eio, [ 'server' ], options
