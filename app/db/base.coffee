###*
# @license
# Copyright 2015 The Lovefield Project Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###

###*
# @typedef {{
#  cities: !Array<string>,
#  colors: !Array<string>,
#  disciplines: !Array<string>,
#  events: !Array<string>,
#  genders: !Array<string>,
#  years: !Array<number>
# }}
# @private
###

ColumnDomains_ = undefined

###*
# @typedef {{
#   city: lf.schema.Column,
#   color: lf.schema.Column,
#   country: lf.schema.Column,
#   discipline: lf.schema.Column,
#   eventGender: lf.schema.Column,
#   event: lf.schema.Column,
#   firstName: lf.schema.Column,
#   gender: lf.schema.Column,
#   lastName: lf.schema.Column,
#   sport: lf.schema.Column,
#   year: lf.schema.Column
# }}
# @private
###

MedalSchema_ = undefined

###*
# @param {!angular.$http} $http
# @constructor
###

DbService = ($http) ->
  @http_ = $http
  @db = null

  ###* @private {!IThenable<!lf.Database>} ###

  @whenInitialized_ = @init_().then((->
    @db
  ).bind(this))
  return

###*
# Builds the database schema.
# @return {!lf.schema.Builder}
# @private
###

main = ->
  app = angular.module('ngDb', [])
  app.service 'DbService', DbService
  app.service 'ResultsService', ResultsService
  app.controller 'ResultsController', [
    'ResultsService'
    ResultsController
  ]
  app.controller 'QueryBuilderController', [
    '$scope'
    '$http'
    'DbService'
    'ResultsService'
    QueryBuilderController
  ]
  return

DbService::buildSchema_ = ->
  schemaBuilder = lf.schema.create('olympia', 1)
  schemaBuilder.createTable('Medal').addColumn('city', lf.Type.STRING).addColumn('color', lf.Type.STRING).addColumn('country', lf.Type.STRING).addColumn('discipline', lf.Type.STRING).addColumn('eventGender', lf.Type.STRING).addColumn('event', lf.Type.STRING).addColumn('firstName', lf.Type.STRING).addColumn('gender', lf.Type.STRING).addColumn('lastName', lf.Type.STRING).addColumn('sport', lf.Type.STRING).addColumn('year', lf.Type.NUMBER).addIndex('idx_year', [ 'year' ]).addIndex 'idx_lastName', [ 'lastName' ]
  schemaBuilder

###*
# Ensures that database is populated with data and initializes the DB
# connection.
# @return {!IThenable}
# @private
###

DbService::init_ = ->
  @buildSchema_().connect().then(((database) ->
    @db = database
    window.db = database
    @checkForExistingData_()
  ).bind(this)).then ((dataExist) ->
    if dataExist then Promise.resolve() else @insertData_()
  ).bind(this)

###*
# Gets the db connection.
# @return {!IThenable.<!lf.Database>}
###

DbService::get = ->
  @whenInitialized_

###*
# Checks if any data exists already in the DB.
# @return {boolean}
# @private
###

DbService::checkForExistingData_ = ->
  medal = @db.getSchema().table('Medal')
  @db.select().from(medal).exec().then (rows) ->
    rows.length > 0

###*
# Inserts data to the DB.
# @return {!IThenable}
# @private
###

DbService::insertData_ = ->
  medal = @db.getSchema().table('Medal')
  @http_.get('../data/olympic_medalists.json').then (response) ->
    rows = response.data.map((obj) ->
      medal.createRow obj
    )
    @db.insert().into(medal).values(rows).exec()

###* @constructor ###

ResultsService = ->

  ###* @private {!Array<!Object>} ###

  @results_ = []
  return

###* @return {!Array<!Object>} ###

ResultsService::getResults = ->
  @results_

###* @param {!Array<!Object>} results ###

ResultsService::setResults = (results) ->
  @results_ = results
  headerData = if results.length > 0 then Object.keys(results[0]).map(((columnName) ->
    columnOptions = 
      field: columnName
      title: columnName
    if columnName == 'color'

      columnOptions.cellStyle = ((value) ->
        { classes: [ value.toLowerCase() ] }
      )
    columnOptions
  )) else []

  ###* @type {{bootstrapTable: !Function}} ###

  $('#results-table').bootstrapTable 'destroy'

  ###* @type {{bootstrapTable: !Function}} ###

  $('#results-table').bootstrapTable
    columns: headerData
    data: results
    pageSize: 25
    pagination: true
    paginationVAlign: 'top'
    sortable: false
  return

###*
# @param {!angular.Scope} $scope
# @param {!angular.$http} $http
# @param {!DbService} dbService
# @param {!ResultsService} resultsService
# @constructor
###

QueryBuilderController = ($scope, $http, dbService, resultsService) ->
  @scope_ = $scope
  @http_ = $http
  @dbService_ = dbService
  @resultsService_ = resultsService
  @fromYears = []
  @toYears = []
  @cities = []
  @disciplines = []
  @events = []
  @countries = []
  @genders = []
  @colors = []
  @sqlQuery = ''
  @unboundValue = undefined
  @populateUi_()
  @addGlobalMethod_()
  return

###*
# Add a global method to be used during live-demos.
# @private
###

QueryBuilderController::addGlobalMethod_ = ->
  window.displayResults = ((results) ->
    @resultsService_.setResults results
    @scope_.$apply()
    return
  ).bind(this)
  return

###*
# Clears all predicates and results.
###

QueryBuilderController::clear = ->
  # Removing all predicates.
  @scope_.citySelection = @unboundValue
  @scope_.disciplineSelection = @unboundValue
  @scope_.countrySelection = @unboundValue
  @scope_.genderSelection = @unboundValue
  @scope_.colorSelection = @unboundValue
  @scope_.fromYearSelection = @unboundValue
  @scope_.toYearSelection = @unboundValue
  @scope_.eventSelection = @unboundValue
  # Removing last results, if any.
  @resultsService_.setResults []
  # Clearing SQL query.
  @scope_.sqlQuery = ''
  return

###*
# @return {!IThenable}
# @private
###

QueryBuilderController::populateUi_ = ->
  @http_.get('../data/column_domains.json').then ((response) ->
    domains = response.data
    @fromYears = domains.years
    @toYears = domains.years
    @cities = domains.cities
    @disciplines = domains.disciplines
    @countries = domains.countries
    @genders = domains.genders
    @colors = domains.colors
    @events = domains.events
    return
  ).bind(this)

###*
# Searches the database based on the query built by the user.
###

QueryBuilderController::search = ->
  @buildQuery_().then(((query) ->
    @scope_.sqlQuery = query.toSql()
    query.exec()
  ).bind(this)).then ((results) ->
    @resultsService_.setResults results
    return
  ).bind(this)
  return

###*
# @param {!MedalSchema_} medal
# @return {?lf.Predicate}
# @private
###

QueryBuilderController::getPredicates_ = (medal) ->
  predicates = []
  if @scope_.countrySelection != @unboundValue
    predicates.push medal.country.eq(@scope_.countrySelection)
  if @scope_.colorSelection != @unboundValue
    predicates.push medal.color.eq(@scope_.colorSelection)
  if @scope_.citySelection != @unboundValue
    predicates.push medal.city.eq(@scope_.citySelection)
  if @scope_.genderSelection != @unboundValue
    predicates.push medal.gender.eq(@scope_.genderSelection)
  if @scope_.disciplineSelection != @unboundValue
    predicates.push medal.discipline.eq(@scope_.disciplineSelection)
  if @scope_.eventSelection != @unboundValue
    predicates.push medal.event.eq(@scope_.eventSelection)
  if @scope_.fromYearSelection != @unboundValue and @scope_.toYearSelection != @unboundValue
    minYear = Math.min(@scope_.fromYearSelection, @scope_.toYearSelection)
    maxYear = Math.max(@scope_.fromYearSelection, @scope_.toYearSelection)
    predicates.push medal.year.between(minYear, maxYear)
  else if @scope_.fromYearSelection != @unboundValue
    predicates.push medal.year.gte(@scope_.fromYearSelection)
  else if @scope_.toYearSelection != @unboundValue
    predicates.push medal.year.lte(@scope_.toYearSelection)
  if predicates.length > 0 then lf.op.and.apply(null, predicates) else null

###*
# @return {!IThenable<!lf.query.SelectBuilder>}
# @private
###

QueryBuilderController::buildQuery_ = ->
  @dbService_.get().then ((db) ->
    medal = db.getSchema().table('Medal')
    predicates = @getPredicates_(medal)
    query = if predicates != null then db.select().from(medal).where(predicates) else db.select().from(medal)
    query
  ).bind(this)

###*
# @param {!ResultsService} resultsService
#
# @constructor
###

ResultsController = (resultsService) ->
  @resultsService_ = resultsService
  return

###*
# @return {!Array<!Object>}
###

ResultsController::getResults = ->
  @resultsService_.getResults()

main()
