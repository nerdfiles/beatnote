define [], () ->

  routes = ($locationProvider) ->

    $routeProvider
      .when("/home", angularAMD.route({
        templateUrl: 'views/home.html',
        controllerUrl: 'controller/home_ctrl'
      }))

      .otherwise({redirectTo: "/home"})

  return routes

