# TECH

    We node.
    We are MEAN.

## Front End

The Front End strategy we take follows the MOVE on paradigm. You might be 
familiar with MVC. We’re not doing MVC. So take the time to understand MVC, 
if you haven’t reviewed it. This is MOVE:

    1. Models
    2. Operations
    3. Views
    4. Events

The task, of course, is getting this to work in AngularJS’s “Way” structure.

    1. Providers
    2. Controllers
    3. Services
    4. Factories
    5. Directives
    6? Run
    7? $$interceptors
    8X Filters
    9. Modules???

That’s the meat of it. Don’t go peaking around the factory. AngularJS switched 
to ES6 Modules for a reason. This is a solution for non-AngularJS 2.x Legacy 
Clients, who may wish to migrate from angularAMD, ui-router, etc. as <2.x.

We’ll ignore that Angularites call just about everything a Service. All $methods 
are services. Config vars are services. Factories are services. Who knows?

Our paradigm blends AngularJS controllers in such a way that they should stand 
as nothing more than interfaces for the more interesting parts, by way of AMD 
and Events and Operations. Models should be their own thing, with their own 
Templates, which may contain Directives. In the end, with this strategy, we 
can see more easily Modules (as Controller Interfaces) and Models isomorphically.

Let’s put it this way: All that declarative talk is great. Do that in Operations. 
Write sane JavaScript, not complicated Directives.

### Model Layer

https://github.com/darlanalves/gisele within angularfire angularAMD modules[0].

We follow a module controller pattern. AngularJS is utilized to redress models 
as multiply realizable components as Directives within more traditional directives 
which treat https://schema.org/WebPage, etc.

—
[0]: http://plnkr.co/edit/LoelLSh54ZEzmSopSU7F?p=info

### REST

https://github.com/DennisAhaus/node-express-crud-router with Blaze Security YAML Spec.

Services interface with HTTP layer.

### Client

Lamport Hash of Last Receipt of Last Connected Client with Off-chain Store for Merkle 
Session FSM in angular-ui-router views with models as per Model Layer.

Use distributed dual navigator routes[0].

—
[0]: https://github.com/plediii/dual-navigator

## Backend

### MITMf

### echoprint

### pyethereum

### Dashboard

django-cms, Django REST Framework for Publishing Internal Node.js API or Firebase 
Repositories
